﻿using UnityEngine;
using System.Collections;

public interface IBoid {
    void SetRepulsiveForceFromFlock(Vector2 fv);
    Vector2 GetPosForFlocking();
}
