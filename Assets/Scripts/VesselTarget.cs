﻿using UnityEngine;
using System.Collections;

/*
 * Moves back and forth.
 */
public class VesselTarget : MonoBehaviour {

    public static VesselTarget Instance { get; private set; }

    private Transform[] path;
    private float speed = 7.21f;
    private int pathIndex;

    public void Awake() {
        Instance = this;
        path = GameController.instance.targetPath;
        pathIndex = 0;
        transform.position = path[pathIndex].position;
    }

    public void Start() {
        StartCoroutine(MoveProcess());   
    }

    private IEnumerator MoveProcess() {

        float t, totalDistance;
        Vector2 start, end;

        while (true) {
            for (int i = 0; i < path.Length; i++) {

                start = path[i].position;
                if (i == path.Length - 1) {
                    end = path[0].position;
                } else {
                    end = path[i + 1].position;
                }

                totalDistance = Vector2.Distance(start, end);
                t = 0;

                while (t/totalDistance < 1) {
                    transform.position = Vector2.Lerp(start, end, t/totalDistance);
                    t += Time.deltaTime * speed;
                    yield return null;
                }
            }
        }
    }

}
