﻿using UnityEngine;
using System.Collections;

public static class FUtils {

    // In degrees
    public static float ComputeRelativeAngle2D(Vector2 posFrom, Vector2 posTo) {
        Vector2 posVW = posTo - posFrom;
        float alphaW = Mathf.Atan2(posVW.y, posVW.x) * Mathf.Rad2Deg;
        return alphaW;
    }
}
