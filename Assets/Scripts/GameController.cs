﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController instance;

    public Camera mainCamera;
    public GameObject vesselPF;
    public GameObject targetPF;
    public Transform[] targetPath;

    private VesselTarget target;
    private Vector2 spawnZoneSize = new Vector2(7f, 3f);
    private int vesselCount = 60;
    private float spawnInterval = 0.02f;

    public void Awake() {
        instance = this;
        target = Instantiate(targetPF).GetComponent<VesselTarget>();
    }

    public void Start() {
        FlockingManager.Instance.Calculate();
        StartCoroutine(SpawnProcess());
    }

    private IEnumerator SpawnProcess() {

        Vector2 spawnPos;

        for (int i = 0; i < vesselCount; i++) {

            spawnPos = new Vector2(
                    Random.Range(-spawnZoneSize.x, spawnZoneSize.x),
                    Random.Range(-spawnZoneSize.y, spawnZoneSize.y));

            Instantiate(vesselPF, spawnPos,
                Quaternion.AngleAxis(
                    FUtils.ComputeRelativeAngle2D(spawnPos, target.transform.position),
                    Vector3.forward));

            yield return new WaitForSeconds(spawnInterval);
        }

    }

    public void OnDestroy() {
        FlockingManager.Instance.StopCalculating();
    }
}
