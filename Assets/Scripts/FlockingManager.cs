﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using System;

/*
 * Manages flocking.
 * Objects (boids) register themselves and receive a vector, sum of the repulsive forces from
 * the flock.
 * The repulsive forces are calculated in a parallel thread and updated regularly.
 * 
 * IMPORTANT
 * 
 *   - Always call 'StopCalculating' when the calculations are not necessary anymore (for
 *   example, at the end of a scene), otherwise the parallel thread will continue to run.
 *   - When an object is not active anymore, remove it with RemoveBoid(). This
 *   implementation doesn't check for null objects (it was used with pooled objects that were
 *   never destroyed)!
 *   - Always call 'StopCalculating' when the program closes (at OnDestroy()). The parallel thread is
 *   a background thread and will be closed when the application closes. However, this doesn't
 *   work in the Unity editor!
 */
public class FlockingManager : IDisposable {
    
    private const int timeBetweenRecalculations = 50;// In milliseconds
    private const float minSqrD = 0.05f;// To avoid a spike in the force when too close

    private static FlockingManager instance;
    public static FlockingManager Instance {
        get {
            if (instance == null) {
                instance = new FlockingManager();
            }
            return instance;
        }
    }

    // Contains the boid and the work flocking vector
    private Dictionary<IBoid, Vector2> boidsForces;
    private HashSet<IBoid> toBeAddedBoids;
    private HashSet<IBoid> toBeRemovedBoids;

    private Thread computationThread;
    private AutoResetEvent stopCalculationsEvent;
    private AutoResetEvent calcThreadFinishedEvent;

    private object synchronizationObj;
    private bool calculating;
    private bool disposed;

    public FlockingManager() {
        stopCalculationsEvent = new AutoResetEvent(false);
        calcThreadFinishedEvent = new AutoResetEvent(true);

        boidsForces = new Dictionary<IBoid, Vector2>();
        toBeAddedBoids = new HashSet<IBoid>();
        toBeRemovedBoids = new HashSet<IBoid>();

        synchronizationObj = new object();
        calculating = false;
    }

    public void AddBoid(IBoid boid) {
        // Verifies if the boid is in the 'to be removed' set
        lock (synchronizationObj) {
            if (toBeRemovedBoids.Contains(boid)) {
                toBeRemovedBoids.Remove(boid);
            } else {
                toBeAddedBoids.Add(boid);
            }
        }
    }

    public void RemoveBoid(IBoid boid) {
        // Verifies if the boid is in the 'to be added' set
        lock (synchronizationObj) {
            if (toBeAddedBoids.Contains(boid)) {
                toBeAddedBoids.Remove(boid);
            } else {
                toBeRemovedBoids.Add(boid);
            }
        }
    }

    public void Calculate() {

        // If calculating = false but threadRunning = true, we received
        // a stop message recently but the thread has not stopped yet.
        // To continue calculating, the stop event is reset with WaitOne(0)
        // and the thread continues like nothing happened.

        bool threadRunning = stopCalculationsEvent.WaitOne(0);
        if (!calculating) {
            if (!threadRunning) {
                computationThread = new Thread(ComputeFlockingVectors);
                computationThread.IsBackground = true;
                computationThread.Start();
            }
            calculating = true;
        }
    }

    /*
     * Must always be called when calculations are no more necessary
     * or when exiting the program.
     */
    public void StopCalculating() {
        if (calculating) {
            stopCalculationsEvent.Set();
            calculating = false;
        }
    }

    private void ComputeFlockingVectors() {

        bool mustStop = false;

        // Waits for the previous calculation thread to finish
        calcThreadFinishedEvent.WaitOne();

        while (!mustStop) {

            /*
             * Updates the boid dictionary with additions and removals
             */
            lock (synchronizationObj) {
                foreach (IBoid b in toBeRemovedBoids) {
                    boidsForces.Remove(b);
                }
                toBeRemovedBoids.Clear();
            }

            lock (synchronizationObj) {
                foreach (IBoid b in toBeAddedBoids) {
                    boidsForces.Add(b, new Vector2(0, 0));
                }
                toBeAddedBoids.Clear();
            }

            /*
             * Calculates the repulsive force for each boid
             */
            float d;
            Vector2 force;
            IBoid cBoid;
            
            if (boidsForces.Count == 1) {
                foreach (IBoid boid in boidsForces.Keys) {
                    boid.SetRepulsiveForceFromFlock(new Vector2(0, 0));
                }
            } else if (boidsForces.Count > 1) {
                // The distance from each boid to every other boid is computed.
                // Calculations can be cut by half if we use the distance from
                // A to B as the distance from B to A.
                // To do this, the boids are moved to a linkedlist, and removed
                // one by one when their calculations are done. The distance
                // calculations are done among boids inside the linked list only.
                LinkedList<IBoid> workList = new LinkedList<IBoid>(boidsForces.Keys);

                foreach (IBoid boid in workList) {
                    boidsForces[boid] = new Vector2(0, 0);
                }

                while (workList.Count > 1) {
                    cBoid = workList.First.Value;
                    workList.RemoveFirst();
                    foreach (IBoid otherBoid in workList) {
                        d = Vector2.SqrMagnitude(otherBoid.GetPosForFlocking() - cBoid.GetPosForFlocking());

                        if (d < minSqrD) {
                            d = minSqrD;
                        }

                        force = (otherBoid.GetPosForFlocking() - cBoid.GetPosForFlocking()).normalized
                            * (1f / d);

                        boidsForces[cBoid] -= force;
                        boidsForces[otherBoid] += force;
                    }

                    cBoid.SetRepulsiveForceFromFlock(boidsForces[cBoid]);
                }

                // Calculations for the last boid in the list are also done
                cBoid = workList.First.Value;
                cBoid.SetRepulsiveForceFromFlock(boidsForces[cBoid]);
            }

            // Sleeps a little, to not spin the cpu
            mustStop = stopCalculationsEvent.WaitOne(timeBetweenRecalculations);
        }

        calcThreadFinishedEvent.Set();
    }

    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing) {
        if (!disposed) {
            if (disposing) {
                stopCalculationsEvent.Close();
                calcThreadFinishedEvent.Close();
            }
        }
        //dispose unmanaged resources
        disposed = true;
    }
}
