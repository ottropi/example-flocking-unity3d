﻿using UnityEngine;
using System.Collections;

/*
 * Tries to reach the VesselTarget and is repulsed by other Vessels.
 */
public class Vessel : MonoBehaviour, IBoid {

    private VesselTarget target;
    private Vector2 flockForce;
    private Rigidbody2D rb;
    private Vector2 flockingPosition;

    public void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    public void Start() {
        target = VesselTarget.Instance;
        flockingPosition = transform.position;
        FlockingManager.Instance.AddBoid(this);
    }
    
    public void Update() {

        // Rotate towards target
        transform.rotation = Quaternion.RotateTowards(
            transform.rotation,
            Quaternion.AngleAxis(
                FUtils.ComputeRelativeAngle2D(transform.position, target.transform.position),
                Vector3.forward),
            2.5f);

        // Towards front of vessel
        rb.AddForce(transform.right * 10, ForceMode2D.Force);

        // Flocking force
        rb.AddForce(flockForce * 0.25f, ForceMode2D.Force);

        // The transform position is only accessible from the main thread
        // To send it to the flocking manager, we must continually update
        // a Vector2 with its value
        flockingPosition = transform.position;
    }

    public void SetRepulsiveForceFromFlock(Vector2 fv) {
        flockForce = fv;
    }

    public Vector2 GetPosForFlocking() {
        return flockingPosition;
    }
}
